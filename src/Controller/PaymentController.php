<?php

namespace Drupal\trinion_cart\Controller;

use Drupal\node\Entity\Node;
use Drupal\trinion_tp\Controller\SozdanieSchetaKlienta;
use Drupal\trinion_tp\TrinionHelper;

/**
 * Trinion cart Payment service.
 */
class PaymentController {
  public function processPayment(Node $order, $sum, $payment_method, $transaction_num = '') {
    $default_timezone = new \DateTimeZone(date_default_timezone_get());
    $now = new \DateTime('now', $default_timezone);
    $order->field_ts_data_oplaty = $now->format('Y-m-d\TH:i:s');
    $order->field_tc_sposob_oplaty = $payment_method;
    $order->field_tp_utverzhdeno = TRUE;
    $order->save();

    $schet = new SozdanieSchetaKlienta();
    $schet = $schet->createSchet($order);

    $stroka = Node::create([
      'type' => 'tp_stroka_schet_klienta_k_oplate',
      'title' => \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('tp_stroka_schet_klienta_k_oplate'),
      'uid' => $uid = \Drupal::currentUser()->id(),
      'status' => 1,
      'field_tp_oplachennaya_summa' => $sum,
      'field_tp_schet' => $schet->id(),
    ]);
    $stroka->save();

    $node = Node::create([
      'type' => 'poluchennyy_platezh',
      'title' => \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('poluchennyy_platezh'),
      'uid' => $uid = \Drupal::currentUser()->id(),
      'status' => 1,
      'field_tp_data' => $schet->get('field_tp_data')->getString(),
      'field_tp_nomer_vkh_plat' => $transaction_num,
      'field_tp_otvetstvennyy' => $order->get('field_tp_otvetstvennyy')->getString(),
      'field_tp_platezhnaya_sistema' => \Drupal::config('trinion_tp.settings')->get('oplata_po_karte_tid'),
      'field_tp_platelschik' => $schet->get('field_tp_schet_dlya')->getString(),
      'field_tp_itogo' => $sum,
      'field_tp_stroki' => $stroka->id(),
    ]);
    $node->save();
    return $order;
  }
}
