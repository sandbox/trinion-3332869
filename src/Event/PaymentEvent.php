<?php

namespace Drupal\trinion_cart\Event;

use Drupal\node\Entity\Node;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Событие оплаты
 */
class PaymentEvent extends Event {

  /**
   * Успешная оплата
   */
  const PAYMENT_SUCCESS = 'trinion_cart.payment.success';

  /**
   * Объект заказа
   */
  protected $node;

  public function __construct(Node $node) {
    $this->node = $node;
  }

  /**
   * Возвращаю объект заказа
   */
  public function getOrder() {
    return $this->node;
  }

}

