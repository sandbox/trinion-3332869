<?php

namespace Drupal\trinion_cart\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Configure Trinion store settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_cart_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_cart.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = $this->config('trinion_cart.settings')->get('default_responsible');
    if ($user)
      $user = User::load($user);
    $form['default_responsible'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Ответственный по умолчанию',
      '#description' => 'Будет подставлен в поле Ответственный пользователь в Заказе клиента',
      '#default_value' => $user,
      '#target_type' => 'user',
    ];
    $cena_dlia_anonima = $this->config('trinion_cart.settings')->get('cena_dlia_anonima');
    if ($cena_dlia_anonima)
      $cena_dlia_anonima = Term::load($cena_dlia_anonima);
    $form['cena_dlia_anonima'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Тип цены для анонимного пользователя',
      '#target_type' => 'taxonomy_term',
      '#default_value' => $cena_dlia_anonima,
      '#selection_settings' => [
        'target_bundles' => ['tip_ceny'],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_cart.settings')
      ->set('default_responsible', $form_state->getValue('default_responsible'))
      ->set('cena_dlia_anonima', $form_state->getValue('cena_dlia_anonima'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
