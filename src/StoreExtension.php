<?php

namespace Drupal\trinion_cart;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StoreExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new TwigFunction('getCartData', [$this, 'getCartData']),
    );
  }

  public function getCartData() {
   return \Drupal::service('trinion_cart.cart')->getCartData();
  }
}
