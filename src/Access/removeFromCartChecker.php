<?php

namespace Drupal\trinion_cart\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка удаляемой из корзины сущности
 */
class removeFromCartChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, $nid) {
    $cart = \Drupal::service('trinion_cart.cart')->getCartData();
    foreach ($cart['items'] as $item) {
      if ($item['cart_item_id'] == $nid)
        return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
