<?php

namespace Drupal\trinion_cart\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка типа добавляемой в корзину сущности
 */
class addToCartOneChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, $nid) {
    $node = Node::load($nid);
    if ($node) {
      $product_types = \Drupal::config('trinion_tp.settings')->get('product_bundles');
      if (in_array($node->bundle(), $product_types)) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }
}
