<?php

namespace Drupal\trinion_cart\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка наличия товаров в корзине
 */
class CartEmptyChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route) {
    $cart = \Drupal::service('trinion_cart.cart')->getCartData();
    return $cart['items'] ? AccessResult::allowed() : AccessResult::forbidden();
  }
}
