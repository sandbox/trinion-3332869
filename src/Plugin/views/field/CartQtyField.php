<?php

namespace Drupal\trinion_cart\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Cart qty field field handler.
 *
 * @ViewsField("cart_qty_field")
 */
class CartQtyField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return [
      '#theme' => 'cart_qty_field',
      '#qty' => $values->_entity->get('field_tp_kolichestvo')->getString(),
      '#nid' => $values->_entity->get('field_tp_tovar')->getString(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }
}
