<?php

namespace Drupal\trinion_cart\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Cart total field field handler.
 *
 * @ViewsField("cart_total_field")
 */
class CartTotalField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return [
      '#markup' => $values->_entity->get('field_tp_kolichestvo')->getString() * $values->_entity->get('field_tp_cena')->getString(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }
}
