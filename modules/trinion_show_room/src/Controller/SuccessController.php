<?php

namespace Drupal\trinion_show_room\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class SuccessController extends ControllerBase {
    public function build() {
        $build['#markup'] = 'Мы свяжемся с вами в ближайшее время.';
        return $build;
    }
}
